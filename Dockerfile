FROM node:7.8

#WORKDIRECTORY
RUN mkdir -p /usr/src/fakeserver
WORKDIR /usr/src/fakeserver

#DEPENDENCIES
COPY package.json /usr/src/fakeserver/
RUN npm install

#SOURCECODE
COPY . /usr/src/fakeserver

EXPOSE 7070
CMD [ "node", "app.js" ]