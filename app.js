const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
var querystring = require("querystring");


const app = express();
app.set('views', 'assets');
app.set('view engine', 'pug');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', function (req, res) {
    res.render('index', { response: "" })
})

app.post('/process', function (req, res) {
    http.get({ host: 'fakeservice', port: 2020, 
        path: '/process?' + querystring.stringify({ rawtext: req.body.rawtext }) },
        function (response) {
            response.on('data', function (d) {
                res.render('index', { response: d })
            });

        }
    );
})
app.listen(3000, function () {
    console.log('Example app listening on port 3000!')
})