# Fake express application

It does renders a page with a form that sends a text to be processed to a server running in fake\_server\_ip.

## Docker
### Docker build
> docker build -t kimy82/fakeexpress .
### Dokcer run
> dokcer run -it -p 3000:3000 --add-host fakeservice:192.168.99.100 kimy82/fakeexpress

## Kubernetes
> kubectl create express-deployment.yaml
> kubectl create express-service.yaml

NOTE: no need to pass fakeservice ip when using kubernetes as 'fakeservice' is already the name of the backend service inside the cluster.
